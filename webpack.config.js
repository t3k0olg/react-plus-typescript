const HtmlWebPackPlugin = require('html-webpack-plugin')
module.exports = {
    entry: "./src/index.tsx",
    devtool: "eval-soruce-map",
    resolve: {
        extensions: ['.js','.ts','.tsx']
    },
    module: {
        rules: [
            {
                test: /\.tsx?$/,
                loader: 'babel-loader',
                exclude: /node_modules/
            },
        ],
    },
    plugins: [
        new HtmlWebPackPlugin({
            template: './src/index.html',
            filename: 'index.html'
        })
    ],
}